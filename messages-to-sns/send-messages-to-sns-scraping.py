import boto3
import csv
import json
# Initialize SNS client for Ireland region
session = boto3.Session(
    region_name="eu-central-1"
)
sns_client = session.client('sns')


def lambda_handler(event, context):
    for i in range(10) :
      res = {
        'body':{
          'nombre':i
        }
      }
    # Send message
      response = sns_client.publish(
          TopicArn='arn:aws:sns:eu-central-1:153290269365:test',
          Message=json.dumps(res)
      )

    return 'OK'