from selenium import webdriver
import selenium
#import boto3
import json

import os
import logging
import numpy as np
from datetime import datetime
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait  # To wait for a page to load
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException  # To Handle a timeout situation
from selenium.webdriver.common.by import By

from os import listdir
from os.path import isfile, join

#dynamodb = boto3.resource('dynamodb')
#table = dynamodb.Table('smovin-immow-data')

def saveItemsToDynamoDb(items):
    for item in items:
        table.put_item(Item ={
                'profile_id':item['profile_id'],
                'source_code': item['source_code']
                })

# utils
def random_sleep(a, b=None):
    if not b:
        b = a
    sleep((b - a) * np.random.random() + a)


class STATUS_CODES:
    SUCCESS = 0
    PAGE_UNAVAILABLE = 1
    LOGIN_FAILED = 2
    ROBOT_CONTROL = 3
    TIMEOUT = 4

class LinkedinScrapper(Chrome):
    def __init__(self, page=None, login="nofak47314@mailres.net", pwd="LittleBigCode", chrome_driver='/opt/chromedriver', options="--incognito"):
        chrome_options = Options()
        chrome_options.binary_location = '/opt/headless-chromium'
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--single-process')
        chrome_options.add_argument('--disable-dev-shm-usage')
        super().__init__(chrome_driver, chrome_options=chrome_options)

        self.linkedin_page = page if page else 'https://www.linkedin.com/in/'
        self.login = login
        self.pwd = pwd
        self.logged_in = False

    # For error verification / utils methods
    def check_page_unavailable(self):  # TODO ADD INTERNETCONNECTION ERROR
        if 'unavailable' in self.current_url:
            return True
        if len(self.find_elements_by_xpath('//h1[@class="error-headline"]')) > 0:
            self.refresh()
            random_sleep(1.5, 2)
            if len(self.find_elements_by_xpath('//h1[@class="error-headline"]')) > 0:
                return True
        return False

    def check_login_or_join_popout(self):
        if len(self.find_elements_by_xpath('//div[@class="flip-card show-login"]')) > 0:
            return True
        if len(self.find_elements_by_xpath('//form[@class="join-form"]')) > 0:
            return True
        return False

    def check_login_verification(self):
        if len(self.find_elements_by_xpath('//*[contains(@id, "email-pin-challenge")]')) > 0:
            return True
        return False

    def check_error_pages(self, abort_on_control=False):
        if self.check_login_or_join_popout():
            return STATUS_CODES.LOGIN_FAILED
        if self.check_page_unavailable():
            return STATUS_CODES.PAGE_UNAVAILABLE
        if self.check_for_control(abort_on_control=abort_on_control):
            return STATUS_CODES.ROBOT_CONTROL
        return STATUS_CODES.SUCCESS

    def check_for_control(self, abort_on_control=False):
        """
        Check if current drive is under captcha control, if it is the case and abort_on_control is True than
        it returns True. Otherwise, if abort_on_control is False, then the scrapper will wait for the user to solve the
        captcha and returns False. If it isn't under captcha control, returns False.

        :param abort_on_control: boolean specifying whether abort or not under control
        :type abort_on_control: bool
        :return: True if under control and indicating to abort. False otherwise.
        :rtype: bool
        """

        random_sleep(0.5, 0.8)
        captcha = self.find_elements_by_xpath('//*[contains(@id, "captcha")]') or 'checkpoint' in self.current_url
        if captcha:
            if abort_on_control:
                return True
            else:
                logging.warning('System asking to fill captcha')
                try:
                    input('Waiting human solving captcha, after the captcha is solver, please press enter')
                    return False
                except EOFError:
                    logging.error('Unable to fill captcha (not interactive)')
                    return True
        return False

    def wait_page_toload(self, By_element, element, timeout=20, abort_on_control=False, ignore_timeout=False):
        """
            Wait for page to load, if ignore_timeout is False, raise Exception.

            Returns True if page correctly loaded
                    False if the page isn't correctly loaded
        """
        if self.check_for_control(abort_on_control=abort_on_control) == STATUS_CODES.ROBOT_CONTROL:
            logging.error('Robot control, aborting profiles scrapping')
            return STATUS_CODES.ROBOT_CONTROL
        try:
            # Wait until the login button is loaded
            WebDriverWait(self, timeout).until(
                EC.visibility_of_element_located((By_element, element)))
            return STATUS_CODES.SUCCESS
        except TimeoutException:
            if not ignore_timeout:
                logging.error(f'TimeOutError: Timed out waiting for page {self.current_url} to load')
                return STATUS_CODES.TIMEOUT
            else:
                return STATUS_CODES.TIMEOUT


    def log_in(self, abort_on_control=True):
        """ Open driver in incognito mode, connects to LinkedIn account and check if under control """
        logging.info('Attempting to login to LinkedIn')
        self.get('https://www.linkedin.com/'
                 'login?fromSignIn=true&trk='
                 'guest_homepage-basic_nav-header-signin')
        status_code = self.wait_page_toload(By.XPATH, '//*[@type="submit"]')
        if status_code == STATUS_CODES.SUCCESS:
            username = self.find_element_by_id('username')
            password = self.find_element_by_id('password')
            username.send_keys(self.login)
            random_sleep(.4, 1.)
            password.send_keys(self.pwd)
            random_sleep(.4, .6)
            log_in_button = self.find_element_by_xpath('//*[@type="submit"]')
            log_in_button.click()
            if self.check_login_verification():
                logging.warning('Linkedin email verification, waiting one minute')
                logging.warning(f'Please verify the email {self.login}')
                pin_check = self.find_element_by_name("pin")
                try:
                    pin_check.send_keys(input('Please enter here your pin: '))
                except EOFError:
                    logging.error('Unable to get pin (not interactive)')
                self.find_element_by_id('email-pin-submit-button').click()
                if self.check_login_verification():
                    logging.critical('Email not verified, aborting scrapping')
                    return STATUS_CODES.LOGIN_FAILED
            status_code = self.check_error_pages(abort_on_control=abort_on_control)
            if status_code == STATUS_CODES.SUCCESS:
                logging.info('Logged into LinkedIn with success')
                self.logged_in = True
                return STATUS_CODES.SUCCESS
            else:
                logging.critical(
                    'Unable to login under robot control (your account may be restricted), trying to scrap '
                    'without login')
            return STATUS_CODES.LOGIN_FAILED
        else:
            logging.critical('Unable to load login page correctly, aborting scrapping')
            return STATUS_CODES.LOGIN_FAILED

    def scroll_page_to_end(self):
        for _ in range(2):
            # Repeat 2 times the scrolling to ensure all page is loaded
            random_sleep(1., 3.)
            self.execute_script(
                "window.scrollTo(0, Math.ceil(document.body.scrollHeight/4));")
            random_sleep(1.5, 2.5)
            self.execute_script(
                "window.scrollTo(0, Math.ceil(document.body.scrollHeight/2.7));")
            random_sleep(1.5, 2.5)
            self.execute_script(
                "window.scrollTo(0, Math.ceil(document.body.scrollHeight/1.5));")
            self.execute_script(
                "window.scrollTo(0, Math.ceil(document.body.scrollHeight));")
        return self
    # --- end error verification / utils methods

    def scrap_linkedin(self, profile_id):
        """ Scrap a profile and save it in a txt file

        :param profile_id: profile id that will be scrapped
        :type profile_id: str
        :param out_path: output directory where the raw html files will be dumped (it will be created if non-existent)
        :type out_path: str
        :return: status code, indicating the success or not of the scrapping (see src.common.helpers.STATUS_CODES)
        """

        self.get(self.linkedin_page + profile_id)
        # check for errors
        status = self.check_error_pages()
        if status == STATUS_CODES.PAGE_UNAVAILABLE:
            logging.warning(f'Page {self.current_url} is unavailable')
            return STATUS_CODES.PAGE_UNAVAILABLE
        if status == STATUS_CODES.LOGIN_FAILED:
            logging.warning(f'LinkedIn asked to login (profile: {profile_id})')
            self.refresh()
            random_sleep(1.5)
            status = self.check_error_pages()
            if status == STATUS_CODES.SUCCESS:
                self.get(self.linkedin_page + profile_id)
            else:
                return status

        self.scroll_page_to_end()
        # Save scrapped raw profile
        item = {'profile_id':profile_id}
        item['source_code'] = self.page_source
        #saveItemsToDynamoDb(item)
        random_sleep(.3, 1.)
        return STATUS_CODES.SUCCESS



def main(event, context):
    profile_id = event['profile_id']

    scrapper = LinkedinScrapper()
    #scrapper.log_in() # Si besoin de logger un compte
    scrapper.scrap_linkedin(profile_id=profile_id)

    body = scrapper.page_source # On récupère juste le code source

    scrapper.close()



    response = {
        "statusCode": 200,
        "body": body
    }
    return response

#if __name__ == '__main__': # Pour tester en local
#   profile_id = 'luizbezerra-pinheiro'
#   main(event = {'profile_id':profile_id},context = None)
